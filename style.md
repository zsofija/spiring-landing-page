## Beskrivning

Hold Norge rusfritt - kjøp spirekasser fra Spiring.no Ved å kjøpe spirekasser fra Spiring.no hjeper du våre ansatte til å holde seg borte fra rusmiljøet de tidligere var en del av. Det er god samfunnsøkonomi, gir bedre menneskeverd og mindre kriminalitet. Kjøp én eller flere spirekasser med umiddelbar levering, eller abonnér på fast levering. Ved abonnement leverer vi jevnlig spirekasse(r) til deg med avtalt leveringsfrekvens.



## Pris & samarbeidspartnere

betaling via vipps, pris per kasse er 250,-

også abonnement

<https://www.linkedin.com/in/olga-popovic-27196b65/>

<https://www.weareonna.no/>

<http://www.byspire.no/>



## Støttespillere:

-  araktaka
-  vaaghals
-  fjøla
-  skansen kullt
-  tratorria popolare.

## Video & Bilder:

 <https://spiring.no/nyheter>



<https://www.pexels.com/hu-hu/foto/elelmiszer-egeszseges-fu-fazek-2694050/>

<https://www.pexels.com/hu-hu/foto/gyar-homaly-fa-orolt-401213/>

<https://www.pexels.com/hu-hu/foto/elelmiszer-gyar-zold-friss-3296644/>



<https://pixabay.com/hu/photos/saatling-pal%C3%A1nta-rettichsprssen-4662630/>

<https://pixabay.com/hu/photos/kihajt-a-z%C3%B6lds%C3%A9geket-retek-3978521/>



## Font
sans Serif - smth like Cubano, Pilcrow Soft

<https://fonts.google.com/specimen/Chango?category=Display,Monospace&thickness=10&preview.text_type=custom>



<https://fonts.google.com/specimen/Londrina+Solid?preview.text_type=custom>



##### logo?

<https://fonts.google.com/specimen/Mansalva?preview.text_type=custom&preview.text=spiring.no>

<https://fonts.google.com/specimen/Barrio?preview.text_type=custom&preview.text=spiring.no>

<https://fonts.google.com/specimen/Kirang+Haerang?preview.text_type=custom&preview.text=SPIRING.no>

<https://fonts.google.com/specimen/Major+Mono+Display?preview.text_type=custom&preview.text=SPIRING.no>

<https://fonts.google.com/specimen/MuseoModerno?preview.text_type=custom&preview.text=SPIRING.no>

## Color palettes
greens! like hsl(169, 71%, 36%) rgb(27, 156, 132)

light yellow: #FEFEE3;



dark green: \#0B3142

light green: #b8cf76

yellow: \#F4E76E

purple: rgb(185, 67, 147)

<https://coolors.co/2c6e49-4c956c-fefee3-ffc9b9-d68c45>

<https://coolors.co/037171-f4e76e-f7fe72-8ff7a7-a1d2ce>

<https://coolors.co/034732-008148-c6c013-c44900-ffffff>

<https://coolors.co/034732-008148-c6c013-f1fffa-d95d39>



forside

#fdf379

headings

<https://fonts.google.com/specimen/Fredoka+One?preview.text_type=custom>

<https://fonts.google.com/specimen/Baloo+2?preview.text_type=custom>

paragraphs

<https://fonts.google.com/specimen/Josefin+Slab?preview.text_type=custom>

<https://fonts.google.com/specimen/Karla?preview.text_type=custom>

<https://fonts.google.com/specimen/Poppins?preview.text_type=custom>

<https://fonts.google.com/specimen/Basic?preview.text_type=custom>

?  <https://fonts.google.com/specimen/Poiret+One?preview.text_type=custom>

<https://fonts.google.com/specimen/Abel?preview.text_type=custom>

\#006d57